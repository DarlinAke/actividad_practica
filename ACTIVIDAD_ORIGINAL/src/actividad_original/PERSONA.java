
package actividad_original;

public class PERSONA {
   
    
    private int segundos;
    
    /**
     * Constructor por defecto
     * @param segundos
     */
    public PERSONA(int segundos){
        this.segundos=segundos;
    }
    /**
     * Devuelve la edad
     * @return Edad acutal
     */
    public int getsegundos() {
        return segundos;
    }

    public void setsegundos(int segundos) {
        this.segundos = segundos;
    }
    
}
    

